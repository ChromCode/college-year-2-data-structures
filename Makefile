# A simple makefile with variables
# takes BusinessSearch.java and favorites.txt
# make myBusiness 
#myBusiness favorites.txt
JAVA1 = greet.java 
JAVA2 = greetings.java 
SOURCES = README makefile ${JAVASRC}
CLASS1 = greet.class 
CLASS2 = greetings.class
MAINCLASS1 = greet
MAINCLASS2 = greetings
JARFILE = runGreet
JARFILE2 = runGreetings
all: ${JARFILE}

${JARFILE}: ${CLASS1}
	echo Main-class: ${MAINCLASS1} > Manifest
	jar cvfm ${JARFILE} Manifest ${CLASS1}
	rm Manifest
	chmod +x ${JARFILE}

${JARFILE2}: ${CLASS2}
	echo Main-class: ${MAINCLASS2} > Manifest
	jar cvfm ${JARFILE2} Manifest ${CLASS2}
	rm Manifest
	chmod +x ${JARFILE2}

${CLASS1}: ${JAVA1}
	javac -Xlint ${JAVA1}	

${CLASS2}: ${JAVA2}
	javac -Xlint ${JAVA2}

clean:
	rm -f ${CLASS1} ${JARFILE}
	rm -f ${CLASS2} ${JARFILE2}
.PHONY: clean ${JARFILE} ${JARFILE2}