// dllistTest.java
// Unit tests for dllist

import org.junit.*;
import static org.junit.Assert.assertEquals;

public class dllistTest {

    @Test
    public void startsEmptyTest() {
        dllist theList = new dllist();
        assertEquals(true, theList.isEmpty());
    }
    
    @Test 
    public void emptyTestFails() {
    	dllist theList = new dllist();
    	theList.insert("String", dllist.position.LAST);
    	assertEquals(false, theList.isEmpty());
    }
    
    @Test 
    public void getItemTest() {
    	dllist theList = new dllist();
    	theList.insert("String", dllist.position.LAST);
    	assertEquals("String", theList.getItem());
    }
    
    @Test 
    public void insertLastTest() {
    	dllist theList = new dllist();
    	theList.insert("String", dllist.position.LAST);
    	theList.insert("Bike", dllist.position.LAST);
    	assertEquals("Bike", theList.getItem());
    }
    
    @Test 
    public void insertFirstTest() {
    	dllist theList = new dllist();
    	theList.insert("String", dllist.position.FIRST);
    	theList.insert("Bike", dllist.position.FIRST);
    	assertEquals("Bike", theList.getItem());
    }
    
    @Test 
    public void setPositionFIRST() {
       dllist theList = new dllist();
       theList.insert("String", dllist.position.LAST);
       theList.insert("Bike", dllist.position.LAST);
       theList.setPosition(dllist.position.FIRST);
       assertEquals("String", theList.getItem());
    }
    
    @Test 
    public void setPositionLAST() {
       dllist theList = new dllist();
       theList.insert("String", dllist.position.FIRST);
       theList.insert("Bike", dllist.position.FIRST);
       theList.setPosition(dllist.position.LAST);
       assertEquals("String", theList.getItem());
    }
    
    @Test 
    public void insertBEFORE() {
       dllist theList = new dllist();
       theList.insert("A", dllist.position.LAST);
       theList.insert("B", dllist.position.LAST);
       theList.insert("C", dllist.position.LAST);
       theList.insert("D", dllist.position.PREVIOUS);
       assertEquals("D", theList.getItem());
    }
    
    @Test 
    public void insertAFTER() {
       dllist theList = new dllist();
       theList.insert("A", dllist.position.FIRST);
       theList.insert("B", dllist.position.FIRST);
       theList.insert("C", dllist.position.FIRST);
       theList.insert("D", dllist.position.FOLLOWING);
       assertEquals("D", theList.getItem());
   }
   
    @Test 
    public void setPositionCompound() {
       dllist theList = new dllist();
       theList.insert("A", dllist.position.LAST);
       theList.insert("B", dllist.position.LAST);
       theList.insert("C", dllist.position.LAST);
       theList.setPosition(dllist.position.FIRST);
       assertEquals("A", theList.getItem());
       theList.setPosition(dllist.position.FOLLOWING);
       assertEquals("B", theList.getItem());
       theList.insert("D", dllist.position.PREVIOUS);
       theList.setPosition(dllist.position.PREVIOUS);
       assertEquals("A", theList.getItem());
       theList.setPosition(dllist.position.FOLLOWING);
       theList.setPosition(dllist.position.FOLLOWING);
       assertEquals("B", theList.getItem());
       theList.insert("E", dllist.position.FOLLOWING);
       theList.setPosition(dllist.position.LAST);
       theList.setPosition(dllist.position.PREVIOUS);
       assertEquals("E", theList.getItem());
    }
    
    @Test 
    public void getPositionCompound() {
       dllist theList = new dllist();
       theList.insert("A", dllist.position.LAST);
       theList.insert("B", dllist.position.LAST);
       theList.insert("C", dllist.position.LAST);
       theList.setPosition(dllist.position.FIRST);
       assertEquals(0, theList.getPosition());
       theList.setPosition(dllist.position.FOLLOWING);
       assertEquals(1, theList.getPosition());
       theList.insert("D", dllist.position.PREVIOUS);
       theList.setPosition(dllist.position.PREVIOUS);
       assertEquals(0, theList.getPosition());
       theList.setPosition(dllist.position.FOLLOWING);
       theList.setPosition(dllist.position.FOLLOWING);
       assertEquals(2, theList.getPosition());
       theList.insert("E", dllist.position.FOLLOWING);
       theList.setPosition(dllist.position.LAST);
       theList.setPosition(dllist.position.PREVIOUS);
       assertEquals(3, theList.getPosition());
    }
    
    @Test
    public void deleteTest() {
       dllist theList = new dllist();
       theList.insert("A", dllist.position.LAST);
       theList.insert("B", dllist.position.LAST);
       theList.insert("C", dllist.position.LAST);
       theList.setPosition(dllist.position.FIRST);
       theList.insert("D", dllist.position.FOLLOWING);
       theList.setPosition(dllist.position.FIRST);
       theList.delete();
       assertEquals("D", theList.getItem());
       theList.setPosition(dllist.position.LAST);
       theList.delete();
       assertEquals("B", theList.getItem());
       theList.setPosition(dllist.position.PREVIOUS);
       assertEquals("D", theList.getItem());
   }  
   
   @Test 
   public void newDeleteTest() {
      dllist theList = new dllist();
      theList.insert("A", dllist.position.LAST);
      theList.insert("B", dllist.position.LAST);
      theList.insert("C", dllist.position.LAST);
      theList.setPosition(dllist.position.FIRST);
      
      theList.delete();
      theList.delete();
      theList.delete();
      assertEquals(true, theList.isEmpty());
   }
   
   @Test(expected=Exception.class)
   public void IllegalArgumentTestFollowing() {
      dllist theList = new dllist();
      theList.insert("A", dllist.position.FOLLOWING);
   }
   
   @Test(expected=Exception.class)
   public void IllegalArgumentTestPrevious() {
      dllist theList = new dllist();
      theList.insert("A", dllist.position.PREVIOUS);
   }
}