// edfile.java
// Template for a line-oriented text editor inspired by ed.
// be sure to actually read the README file

import java.util.Scanner;
import static java.lang.System.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.io.*;

class edfile{

   public static void main (String[] args)  {
      System.out.println("Welcome to the program");
      
       Scanner sc = new Scanner(System.in);
      
      boolean want_echo = false;
      String file, filename, text, wFile;
      int NumOfLines = 0;
      int NewLines = 0;
      int ErrorStatus=0;
      dllist lines= new dllist();
      dllist newLines= new dllist();
      if(args[0].equals("-e")) {
         //System.out.println("Options selected: -e");
         want_echo = true;
      }
      try {
        file = args[1];
      } catch ( ArrayIndexOutOfBoundsException e) {
        file = "none";
      }
      //System.out.println("Operand given: "+file);
      
      try {
      	BufferedReader in = new BufferedReader(new FileReader(file));
      	while(true) {
   	      String object = in.readLine();
   	      if(object == null) break;
   	      NumOfLines++;
   	      newLines.insert(object, dllist.position.LAST);
         }
      } catch (IOException e) {  }// do nothing 
      
      //System.out.prinln(object);
    
      Scanner stdin = new Scanner (in);
      Scanner addText = new Scanner(System.in);
      for (;;) {
         if (! stdin.hasNextLine()) break;
         String inputline = stdin.nextLine();
         if (want_echo) out.printf ("%s%n", inputline);
         if (inputline.matches ("^\\s*$")) continue;
         char command = inputline.charAt(0);
         switch (command) {
            case '#': break;
            case '$': 
                newLines.setPosition(dllist.position.LAST); 
                System.out.println(newLines.getItem()); break;
            case '*': 
                newLines.setPosition(dllist.position.LAST);
                int i = newLines.getPosition();
                newLines.setPosition(dllist.position.FIRST);
                for(int j = 0; j <= i; j++) {
                   if(j == 0) {
                      newLines.setPosition(dllist.position.FIRST);
                      System.out.println(newLines.getItem());
                   } else { 
                      newLines.setPosition(dllist.position.FOLLOWING);
                      System.out.println(newLines.getItem());
                   }
                }    
            	break;
            case '.': 
                System.out.println(newLines.getItem()); break;
            case '0': newLines.setPosition(dllist.position.FIRST); 
                      System.out.println(newLines.getItem()); break;
            case '<': 
                if(newLines.getPosition() == 0) break;
            	newLines.setPosition(dllist.position.PREVIOUS); 
            	System.out.println(newLines.getItem()); break;
            case '>':  
                if(newLines.getPosition() == NumOfLines-1) break;
            	newLines.setPosition(dllist.position.FOLLOWING); 
            	System.out.println(newLines.getItem()); break;
            case 'a': 
                text = addText.nextLine();
            	newLines.insert(text, dllist.position.FOLLOWING);
            	NumOfLines++; 
            	System.out.println(newLines.getItem()); break;
            case 'd': newLines.delete(); NumOfLines--; break;
            case 'i': text = addText.nextLine();
            	newLines.insert(text, dllist.position.PREVIOUS); 
            	System.out.println(newLines.getItem());
            	NumOfLines++;
            	break;
            case 'r':
            	try { 
            	    filename = addText.next();
            		BufferedReader inA = new BufferedReader(new FileReader(filename));
      	            while(true) {
   	      			   String object1 = inA.readLine();
   	      			   if(object1 == null) break;
   	      			   NumOfLines++;
   	      			   NewLines++;
   	      			   newLines.insert(object1, dllist.position.FOLLOWING);
   	      			   //System.out.println(object1);
   	      		    }
            	} catch (IOException e) { 
            		System.out.println("The file could not be read");
            		ErrorStatus = 1;
            	}
            	System.out.println(NewLines); break; 
            case 'w': 
            	   try {
            	      wFile = addText.next();
            	      File wCase = new File(wFile);
            	      FileWriter writer = new FileWriter(wFile);
            	      newLines.setPosition(dllist.position.LAST);
                      int k = newLines.getPosition();
                	  newLines.setPosition(dllist.position.FIRST);
                	  for(int j = 0; j <= k; j++) {
                         if(j == 0) {
                            newLines.setPosition(dllist.position.FIRST);
                      	    writer.write(newLines.getItem()+"\n");
                   		 } else { 
                            newLines.setPosition(dllist.position.FOLLOWING);
                            writer.write(newLines.getItem()+"\n");
                         }
                      }
            	      writer.flush();
            	      writer.close();
            	      System.out.println(NumOfLines);
                   } catch (IOException e) {
                   System.out.println("File could not be written or created");
                   ErrorStatus = 1;
                }
                break;
            default : System.out.println("Print a valid command next time");
                      System.exit(1); break;
         }
      }
      System.out.println("End of File"); 
      System.out.println("Error Status "+ErrorStatus);
   }
   
   
}

