// dllist.java
// Template code for doubly-linked list of strings.

public class dllist {

   public enum position {FIRST, PREVIOUS, FOLLOWING, LAST};

   private class node {
      String item;
      node prev;
      node next;
      
      public node(String it) {
         item = it;
         prev = null;
         next = null;
      }
   }

   private node first = null;
   private node current = null;
   private node last = null;
   private int currentPosition = 0;
   
   //Constructor
   

   public void setPosition (position pos) {
      if(pos == position.FIRST) current = first;
      if(pos == position.PREVIOUS) current = current.prev;
      if(pos == position.FOLLOWING) current = current.next;
      if(pos == position.LAST) current = last;
    }   

   public boolean isEmpty () {
      return(first == null);
   }

   public String getItem () {
      if( isEmpty() ) throw new java.util.NoSuchElementException();
      return current.item;
   }

   public int getPosition () {
      if( isEmpty() ) throw new java.util.NoSuchElementException();
      node n = first;
      currentPosition = 0;
      for(;n != null; n = n.next) {
         if(n.item == current.item) { 
            //System.out.print(currentPosition);
            return(currentPosition);
         } else {
            currentPosition++;
         }
      }
      return(currentPosition);
   }

   public void delete () {
      if( isEmpty() ) throw new java.util.NoSuchElementException();
         
      if(current == first) {             // found it; first item?
         first = current.next; 
         current = first;
      } else {
         if(current==last) {             // last item?
            last = current.prev;  
            current = last;  // old prev <-- last
         } else {                         
            current.prev.next = current.next;
            current.next.prev = current.prev;
            current = current.prev.next;
         }
      }
   }

   public void insert (String item, position pos) {
      node newNode = new node(item);
      
      //from doublyLinked.java in Examples, modified for my code
      if(pos == position.FIRST) {
         if( isEmpty() )  {              // if empty list,
            last = newNode; 
            current = last;            // newNode <-- last
         } else {
            first.prev = newNode;   // newNode <-- old first
            newNode.next = first;          // newNode --> old first
         }
         first = newNode;  
         current = first;
      }
      
      if(pos == position.PREVIOUS) {
         if( isEmpty() ) {
            System.out.println("The set it empty, can't add to PREVIOUS.");
            throw new IllegalArgumentException(); 
         }   
         if(current == first) {
            current = current; // do nothing
         } else { 
            newNode.prev = current.prev; // newNode  old
            current.prev.next = newNode;
            newNode.next = current;  //newNode <-- old
            current.prev = newNode; 
            current = newNode;       
        }
      }
      
      if(pos == position.FOLLOWING) {
         if( isEmpty() ) {
            System.out.println("The set it empty, can't add to FOLLOWING.");
            throw new IllegalArgumentException(); 
         } 
         if(current == last) {
            current = current;
         } else { 
            newNode.next = current.next; // newNode --> old next
            current.next.prev = newNode;
            newNode.prev = current;    // old current <-- newNode
            current.next = newNode;
            current = newNode;
        }
        //current = newNode;
      }
      
      //from doublyLinked.java in Examples, modified for my code
      if(pos == position.LAST) { 
         if( isEmpty() ) { 
            first = newNode; 
            current = first;
         } else {
            last.next = newNode;        // old last --> newNode
            newNode.prev = last;    // old last <-- newNode
         }
         last = newNode;  
         current = last;      
      }            
   }
}

/*class DllistApp
   {
   public static void main(String[] args)
      {                             // make a new list
      dllist theList = new dllist();

	  System.out.println(theList.isEmpty());  // display list forward

      theList.insert("A", dllist.position.LAST); 
      theList.insert("B", dllist.position.LAST); 
      theList.insert("C", dllist.position.LAST);
      theList.setPosition(dllist.position.FIRST);
      theList.setPosition(dllist.position.FOLLOWING);
      theList.insert("D", dllist.position.prev);
      theList.setPosition(dllist.position.prev);
      
      theList.setPosition(dllist.position.FOLLOWING);
      theList.setPosition(dllist.position.FOLLOWING);
           // insert at front
      System.out.println(theList.getPosition());  // display list forward
      }  // end main()
   }
*/

