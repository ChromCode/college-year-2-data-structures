#include <stdio.h>
#include <stdlib.h>
struct node {
 int v;
 struct node *next;
};
int main(int argc, char *argv[]) {
 struct node *n = malloc(sizeof(struct node));
 n->v = 100;
 n->next = malloc(sizeof(struct node));
 n->next->v = 200;
 n->next->next = n;
 printf("%d\n", n->v + n->next->v + n->next->next->next->v);
 free(n->next);
 free(n);
 return 0;
}