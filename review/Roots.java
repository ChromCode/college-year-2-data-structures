import java.util.Scanner;

class Roots{

	public static void main (String[] args){

		Scanner sc = new Scanner(System.in);
		double resolution = 0.01;
		double tolerance = 0.0000001;
		double threshold = 0.001;
		double R,L; // right-left endpoints
		//double mid;
		//double width;
		int n,i; // n is the degree of the polynomial and i is for loop control
		double[] A,K;
    double r,l; // subintervals
    double PolyRoot = 0, DerRoot = 0;



		System.out.print("Enter the degree: " );
		n = sc.nextInt();
		A = new double[n+1];

		System.out.print("Enter " + (n+1) + " coefficients: ");

		for(i = 0; i < A.length; i++){
			A[i] = sc.nextDouble();
		}
    

		System.out.print("Enter the left and right endpoints: ");
		L = sc.nextInt();
		R = sc.nextInt();

    System.out.print('\n');

    K = diff(A); // contains the coefficients of the derivative

		
    l = L;
    r = l + resolution;

    while(l < R) {
     
     //System.out.println("l = " + l + " " + "r = " + r);

      if(poly(A,r) * poly(A,l) < 0){
        PolyRoot = findRoot(A,l,r,tolerance);
        System.out.print("Odd Root found at: ");
        System.out.printf("%.5f%n", PolyRoot);  
        //System.out.println(PolyRoot);     
      } else if(poly(K,r) * poly(K,l) < 0){
        DerRoot = findRoot(K,l,r,tolerance);
        if(Math.abs(poly(A,DerRoot)) < threshold){
          System.out.print("Even Root found at: " );
          System.out.printf("%.5f%n", DerRoot);
          //System.out.println(DerRoot);
        }
      }
     
      l = r;
      r = l +resolution;
    }

     //System.out.println(PolyRoot); 
     //System.out.println(DerRoot);


    if (PolyRoot == 0 || DerRoot == 0){
      System.out.println("No roots were found in the specified range.");
    }
      
		
  }

      static double poly(double[] C, double x ){
        double sum = 0;
        int i;
        
      	for(i = 0; i < C.length; i++){
      		sum += C[i] * Math.pow(x,i);
      	}
      	return sum;

      } 

      static double[] diff(double[] C){

      	double[] D = new double [C.length-1];
      	int i;

      	for(i = 1; i<C.length-1; i++){
      		D[i] = (i) * C[i];
      	}
        //for(i=0; i<C.length; i++){
          //System.out.print(D[i] + " ");
        //}
      	return D;
      }

      static double findRoot(double [] C, double a, double b, double tolerance){

      	double root = a;
      	double width = b - a;

      	while (width > tolerance) {
      		root = (a+b)/2.0;
      		//residual = poly(C, root);
      		if (poly(C,a) * poly(C,root) < 0){
      			b = root;
      		} else {
      			a = root;
          }
      		width = b - a;
      		  		
      	}
      	return root;
      }
      
}