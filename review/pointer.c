#include <stdio.h>
int main(int argc, char *argv[]) {
 int x = 5;
 int y = 2;
 int *p;
 int *q;
 p = &y;
 q = &x;
 x += 5;
 *q = *p;
 y++;
 printf("x + y + *p + *q = %d\n", x + y + *p + *q);
 return 0;
}