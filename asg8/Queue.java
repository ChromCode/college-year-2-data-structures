
import java.util.Iterator;
import java.util.NoSuchElementException;

class Queue <Item> implements Iterable <Item> {

   private class Node {
      Item item;
      Node next;
   }
   private Node head = null;
   private Node tail = null;

   public boolean isEmpty() {
      if(head == null) return true;
      else { return false; }
   }

   public void insert(Item newItem) {
      Node newNode = new Node();
       newNode.item = newItem;
      if ( isEmpty() ) {
         head = newNode;
      } else { 
         tail.next = newNode;
      }
      tail=newNode;
   }

   public Iterator <Item> iterator() {
      return new Itor ();
   }

   class Itor implements Iterator <Item> {
      Node current = head;
      public boolean hasNext() {
         return current != null;
      }
      public Item next() {
         if (! hasNext ()) throw new NoSuchElementException();
         Item result = current.item;
         current = current.next;
         return result;
      }
      public void remove() {
         throw new UnsupportedOperationException();
      }
   }

}
