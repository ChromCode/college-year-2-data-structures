import java.io.*;
import java.util.Scanner;
import static java.lang.System.*;

class xref {
	//boolean debug;
    static void processFile(String filename, boolean debug) throws IOException {
        Scanner scan = new Scanner (new File(filename));
        Tree tree = new Tree();
        for (int linenr = 1; scan.hasNextLine (); ++linenr) {
            for (String word: scan.nextLine().split ("\\W+")) {
                //out.printf ("%s: %d: %s%n", filename, linenr, word);
                tree.insert(word, linenr);
            }
        }
        scan.close();
        if (debug) {
            tree.debug();
        } else {
            tree.output();
        }
    }

    public static void main(String[] args) {
        String filename = null;
        boolean debug = false;
        try {
           args[0].equals("-d");
        } catch (ArrayIndexOutOfBoundsException e) {
           Error();
        }
         
        try {
           filename = (args[args.length-1]);
        } catch (NumberFormatException e) {
           Error();
        }
        
        if(args[0].equals("-d")) {
           debug = true;
        }  else if(args[0].equals(filename)) {
           //do nothing
        } else {
           Error();
        }

        try {
            processFile(filename, debug);
        }catch (IOException error) {
            auxlib.warn (error.getMessage());
        }
        
    }
    
    static void Error() {
      System.out.println("Usage: [-d] filename ");
      System.out.println("Option: -d debug output, print all nodes in order");
      System.exit(0);
   }

}

