import static java.lang.System.*;
import java.util.*;

class Tree {

    private class Node {
        String key;
        Queue<Integer> value;
        Node left;
        Node right;
        
        Node(String x, int linenum) {
        	key = x;
        	value = new Queue<Integer>();
        	value.insert(linenum);
         }
    }
    private Node root;

    private void debugHelper(Node tree, int depth) {
        // Your code here might be recursive
        String space = "";
        if( tree != null ) {
           for(int i = 0; i < depth; i++) {
              space = space+"  ";
           }
           depth++;
           debugHelper(tree.left, depth);
           System.out.print(space+(depth-1)+(" ")+tree.key+" \n");
           debugHelper(tree.right, depth);
           
        } 
    }

	private void outputHelper(Node tree) {
		if(tree != null) {
	    	outputHelper(tree.left);
    		Iterator<Integer> iterator = tree.value.iterator();
    		System.out.print(tree.key +" : ");
    		while(iterator.hasNext() == true){ 
        		System.out.print(iterator.next()+" "); 
        	}
        	System.out.println();
       	 	outputHelper(tree.right);
			}
		}
	// modified tree.java from Chap08 of textbook
    public void insert(String key, int linenum) {
        // Insert a word into the tree
        Node newNode = new Node(key, linenum);    // make new node with key and linenum

        if(root==null) root = newNode;       // no node in root
        else  {                              // root occupied
           Node current = root;       		// start at root
           Node parent;
           while(true)  {              		// (exits internally)
              parent = current;
              if(key.compareTo(current.key) < 0) { 		// go left?
                // System.out.println(key+" is less than "+current.key);
                 current = current.left;
                 if(current == null) {  		// if end of the line
                    parent.left = newNode;   //insert on the left
                    return;
                 }
              }  // end if go left
              else if (key.compareTo(current.key) > 0) {                  // or go right?
                 current = current.right;
                 if(current == null) {  			// if end of the line                 
                    parent.right = newNode;		// insert on right
                    return;
                 }
              }  // end else go right
              else { 
                 current.value.insert(linenum);
                 return;
              }
            }  // end while
         }  // end else not root
      }  // end insert()

    public void debug() {
        // Show debug output of tree
        debugHelper(root, 0);
    }

    public void output() {
        // Show sorted words with lines where each word appears
        outputHelper(root);
    }

}
