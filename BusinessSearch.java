//BusinessSearch.java
//Uses Binary search (merge sort) to find a business name in an array
//the array is made through user input of a file
//Special Instructions: Usage java BusinessSearch filename


import java.util.Scanner;
import java.io.*;

class BusinessSearch {
	// mergeSort()
   // sort subarray A[p...r]
   // from tantalos cmps12b website, modified for strings instead of ints
   public static void mergeSort(String[] A, int p, int r){
      int q;
      if(p < r) {
         q = (p+r)/2;
         // System.out.println(p+" "+q+" "+r);
         mergeSort(A, p, q);
         mergeSort(A, q+1, r);
         merge(A, p, q, r);
      }
   }

   // merge()
   // merges sorted subarrays A[p..q] and A[q+1..r]
   // from tantalos cmps12b website, modified for strings instead of ints
   public static void merge(String[] A, int p, int q, int r){
      int n1 = q-p+1;
      int n2 = r-q;
      String[] L = new String[n1];
      String[] R = new String[n2];
      int i, j, k;

      for(i=0; i<n1; i++) L[i] = A[p+i];
      for(j=0; j<n2; j++) R[j] = A[q+j+1];
      i = 0; j = 0;
      for(k=p; k<=r; k++){
         if( i<n1 && j<n2 ){
            if( L[i].compareTo(R[j]) < 0 ){
               A[k] = L[i];
               i++;
            }else{
               A[k] = R[j];
               j++;
            }
         }else if( i<n1 ){
            A[k] = L[i];
            i++;
         }else{ // j<n2
            A[k] = R[j];
            j++;
         }
      }
   }
   
   // from tantalo's cmps12b website, modified for strings instead of ints
   public static int binarySearch(String[] A, int p, int r,  String target){
      int q;
      if(p > r) {
         return -1;
      }else{
         q = (p+r)/2;
         if(target.compareTo(A[q]) == 0){
            return q;
         }else if(target.compareTo(A[q]) < 0){
            return binarySearch(A, p, q-1, target);
         }else{ //target > A[q]
            return binarySearch(A, q+1, r, target);
         }
      }
   }
   
   	static void Error() {
   	   System.out.println("Usage: BusinessSearch BusinessDB");
   	   System.exit(0);
   	}
   
	public static void main( String[] args) throws FileNotFoundException{
		String database, input, names, numbers;
		int n = 0, i = 0, x, queries = 0, notFounds = 0;

		try {
         args[0].equals("\n");
      } catch (ArrayIndexOutOfBoundsException e) {
         Error();
      }

		Scanner sc = new Scanner(new File(args[0]));
		Scanner in = new Scanner(System.in); // user input
		
		while(sc.hasNextInt()) {
			n = sc.nextInt(); //
		}
		
		String[] A = new String[n]; //array with whole file names, numbers with length n
		String[] Names = new String[n]; // array with just names
		String[] Numbers = new String[n]; // array with just numbers
		
		sc = new Scanner(new File(args[0]));
		
		while(sc.hasNextLine()) {
			if(sc.hasNextInt()) {
				sc.nextLine(); // skip the line if it has an int
			}
			i++;
			database = sc.nextLine();
			A[i-1] = database; // fill the array with the strings
		}
		
		mergeSort(A, 0, A.length-1);
		
		for(int j=0; j<=A.length-1; j++) {
			names = A[j];
			String[] B = names.split(","); //creates a 1x2 matrix with just name,number
			Names[j] = B[0];    // creates a matrix with just the names
			Numbers[j] = B[1];  // creates a matrix with just the numbers
			//System.out.println(Numbers[j]);
		} 
		
		//System.out.println("Usage: BusinessSearch BusinessDB");
		
		while(true) {
			 if((input = in.nextLine()).isEmpty()) break; // break to the print statement
			x = binarySearch(Names, 0, Names.length-1, input); //Binary search the names
			if(x > 0 || x == 0) { //if the names are in the search
				System.out.println(Numbers[x]);
				queries += 1;
			} else { // not in the search
				System.out.println("NOT FOUND");
				queries += 1;
				notFounds +=1;
			} 
		} // want break to land here
		System.out.println(queries+" total queries, "+notFounds+" not found.");
	}
}