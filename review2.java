import java.util.Scanner;

class review2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("What is your age?");
		int age = sc.nextInt();
		
		if(isLegal(age) == false) {
			System.out.println("You are not over 21.");
		} else { 
			System.out.println("You are legal drinking age.");
		}
	}
	
	public static boolean isLegal(int x) {
		if(x < 21) {
			return false;
		} else {
			return true;
		}
	}
}