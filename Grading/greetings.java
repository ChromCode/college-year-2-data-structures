// greetings.java
// Greet everyone listed in people.txt
// file was changed to directory.txt where you will find a name and a phone number
// greetings.java was updated by adding a split in line 23 so it will read only the name in the file dirctory.txt
// without printing the phone number


import static java.lang.System.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
class greetings {
 public static void main( String[] args ) throws IOException {
 BufferedReader in = new BufferedReader(
 new FileReader("directory.txt"));
 
 
 while(true) {
 String name = in.readLine();
 if (name == null)
 break;
 String[]parts=name.split(","); 
 System.out.println("Hello, " + parts[0] + ".");
 }
 in.close();
 }
}
