// Calc.java
// Define a class for doing RPN.

public class Calc {
	public int top, totalNumbers;
	public double[] Stack;
	
    // Constructor
    public Calc() {
    	Stack = new double[100]; 
    	top = -1;
    }
    
    // Push a number
    public void push(double x) throws RuntimeException  {
    	if(top == 99) { throw new RuntimeException("Stack is full"); }
    	Stack[++top] = x;
    	totalNumbers += 1;
    }
    
    // Pop top number (removes)
    public double pop() throws RuntimeException  {
    	if(top < 0) { throw new RuntimeException("Stack is empty"); }	
    	totalNumbers -= 1;
    	return Stack[top--];
    }
    
    // Peek at top number (does not remove)
    public double peek() throws RuntimeException {
    	if(top < 0) { throw new RuntimeException("Stack is empty"); }
    	return Stack[top];
    }
    
    // Add top two numbers
    public void add() throws RuntimeException {
    	double temp1 = pop();
    	double temp2 = pop();
    	double sum = temp2 + temp1;
    	push(sum);
    	//return Stack[top];
    	
    }
    
    // Subtract top two numbers (top on right side)
    public void subtract() throws RuntimeException {
    	double temp1 = pop();
    	double temp2 = pop();
    	double difference = temp2 - temp1;
    	push(difference);
    	//return Stack[top];
    }

    // Multiply top two numbers
    public void multiply() throws RuntimeException {
    	double temp1 = pop();
    	double temp2 = pop();
    	double product = temp2 * temp1;
    	push(product);
    	//return Stack(top);
    }
    
    // Divide top two numbers (top on bottom)
    public void divide() throws RuntimeException {
    	double temp1 = pop();
    	double temp2 = pop();
    	if(temp2 == 0) { throw new RuntimeException("Can't divide by 0"); } 
    	double div = temp2 / temp1;
    	push(div);
    	//return Stack[top];
    }
    
    //Take the log base 2 of the top number
    public void log() throws RuntimeException {
    	double temp1 = pop();
    	if(temp1 <= 0) { 
    		throw new RuntimeException("Can't take a log of a negative or 0." ); 
    	}
    	double result = Math.log(temp1) / Math.log(2);
    	push(result);
    }
    
    // Return how many numbers are in the stack
    public int depth() {
    	return totalNumbers;
    }
}
