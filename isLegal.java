import java.util.Scanner;

class isLegal {
	public static void main(System[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("What is your age?");
		int age = sc.nextInt();
		
		if(isLegal(age) == false) {
			System.out.println("You are not over 21.");
		} else { 
			System.out.println("You are over 21.");
		}
	}
	
	public static boolean isLegal(int x) {
		if(x < 21) {
			return false;
		} else {
			return true;
		}
	}
}